package com.example.myapplicationtp3;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    public static int nbrThreadsRunningRefresh = 0;
    public static SwipeRefreshLayout swipe;
    private static final int REQUEST_CODE = 1 ;
    public static SportDbHelper dbHelper ;
    private ListView listView;
    private Cursor cursor;
    public static SimpleCursorAdapter adapter;
    public Team team ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        swipe.setOnRefreshListener(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbHelper = new SportDbHelper(this) ;
        dbHelper.populate();

        cursor = dbHelper.fetchAllTeams();
        cursor.moveToFirst();

        //on lie la BD avec La listView
        adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor,
                new String[]{
                        //Tableau de colonnes pour lier le curseur
                        dbHelper.COLUMN_TEAM_NAME,
                        dbHelper.COLUMN_LEAGUE_NAME
                },

                new int[]{
                        //Tableau de colonnes pour lier le curseur
                        android.R.id.text1, android.R.id.text2
                },
                0);

        listView = (ListView) findViewById(R.id.list_team);
        listView.setAdapter(adapter);



        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,
                        NewTeamActivity.class);
                startActivityForResult(intent , REQUEST_CODE);

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View view, int position, long id) {

                Intent intentTeam = new Intent(view.getContext(), TeamActivity.class);
                Cursor cursorTeam = (Cursor) listView.getItemAtPosition(position);
                Team teamS = dbHelper.cursorToTeam(cursorTeam);
                Log.d("Ouverture de TeamA", teamS.toString());
                intentTeam.putExtra("teamSelected", teamS);
                startActivity(intentTeam);
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == REQUEST_CODE  && resultCode  == RESULT_OK) {

                team = data.getParcelableExtra("NewTeamActivity");
                dbHelper.addTeam(team) ;
            }
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.toString(),
                    Toast.LENGTH_SHORT).show();
        }

    }

    /*public void onRefreshListener(){

      for (Team team : MainActivity.dbHelper.getAllTeams()){
          RefreshData refreshData = new RefreshData();
          refreshData.execute(team);
      }


    }*/

    protected void onResume() {
        super.onResume();
        cursor = dbHelper.fetchAllTeams();
        adapter.changeCursor(cursor);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onRefresh() {

        for (Team team : MainActivity.dbHelper.getAllTeams()){
            RefreshData refreshData = new RefreshData();
            refreshData.execute(team);
            MainActivity.nbrThreadsRunningRefresh++;
        }
    }


    public class RefreshData extends AsyncTask<Team,String,Team>{


        @Override
        protected Team doInBackground(Team... teams) {
            JSONResponseHandlerTeam responseTeam = new JSONResponseHandlerTeam(teams[0]);
            JSONResponseHandlerLastEvent responseLastEvents = new JSONResponseHandlerLastEvent(teams[0]);
            JSONResponseHandlerScoreRank responseRanking = new JSONResponseHandlerScoreRank(teams[0]);

            TeamActivity.loadContent(teams[0], responseTeam);
            TeamActivity.loadLastEvent(teams[0], responseLastEvents);
            TeamActivity.loadRank(teams[0], responseRanking);
            MainActivity.dbHelper.updateTeam(teams[0]);

            return null;

        }

        @Override
        protected void onPostExecute(Team team) {
            MainActivity.nbrThreadsRunningRefresh--;
            if (MainActivity.nbrThreadsRunningRefresh <= 0){
                MainActivity.adapter.notifyDataSetChanged();
                MainActivity.swipe.setRefreshing(false);
                Log.d("RefreshData","Finished");
            }
        }

    }




}
