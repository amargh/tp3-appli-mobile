package com.example.myapplicationtp3;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerLastEvent {
    private static final String TAG = JSONResponseHandlerLastEvent.class.getSimpleName();

    private Team team;

    public JSONResponseHandlerLastEvent(Team team) {
        this.team = team;
    }

    public void readJsonStreamLastEvent(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readLastEvent(reader);

        } finally {
            reader.close();
        }
    }

    public void readLastEvent(JsonReader reader) throws IOException {
        reader.beginObject();

        while (reader.hasNext()) {


            String name = reader.nextName();

            if (name.equals("results")) {
                readArrayLastEvent(reader);
            } else {
                reader.skipValue();
            }
        }

        reader.endObject();
    }

    private void readArrayLastEvent(JsonReader reader) throws IOException {


        Match match = new Match();

        // lire la array
        reader.beginArray();

        int nb = 0; // considérer que le premier élément de la array

        // parcourir les last event

        while (reader.hasNext()) {

            reader.beginObject();

            while (reader.hasNext()) {

                String name = reader.nextName();

                // pour le premier match seulement
                if (nb == 0) {

                    // recupérer les informations
                    if (name.equals("strEvent")) {
                        match.setLabel(reader.nextString());
                    } else if (name.equals("strHomeTeam")) {
                        match.setHomeTeam(reader.nextString());
                    } else if (name.equals("strAwayTeam")) {
                        match.setAwayTeam(reader.nextString());
                    } else if (name.equals("intHomeScore")) {
                        if (reader.peek() != JsonToken.NULL) {
                            match.setHomeScore(reader.nextInt());
                        } else {
                            match.setHomeScore(+1);
                            reader.skipValue();
                        }
                    } else if (name.equals("intAwayScore")) {
                        if (reader.peek() != JsonToken.NULL) {
                            match.setAwayScore(reader.nextInt());
                        }else{
                            match.setAwayScore(+1);
                            reader.skipValue();
                        }
                    } else {
                        reader.skipValue();
                    }

                } else {
                    reader.skipValue();
                }
            }

            reader.endObject();

            nb++;
        }

        reader.endArray();


        // information des match
        team.setLastEvent(match);
    }



}
