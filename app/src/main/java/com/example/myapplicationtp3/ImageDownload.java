package com.example.myapplicationtp3;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.net.URL;

public class ImageDownload extends AsyncTask<ImageView, Void, Bitmap> {

    ImageView imageView;
    Team team;

    public ImageDownload(ImageView imageView, Team team) {
        this.imageView = imageView;
        this.team = team;
    }

    @Override
    protected Bitmap doInBackground(ImageView... imageViews) {

        Bitmap b = null;

        try {
            URL url = new URL(this.team.getTeamBadge());
            b = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (Exception e) {
            Log.d("error_badge",e.toString());
        }

        return b;
    }

    @Override
    protected void onPostExecute(Bitmap result) {

        imageView.setImageBitmap(result);

    }
}


