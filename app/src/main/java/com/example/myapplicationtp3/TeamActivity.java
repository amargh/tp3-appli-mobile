package com.example.myapplicationtp3;


import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    public static TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;


    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;

    public static String SUCCESS = "Executed";
    public static String FAIL = "Failed";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = (Team) getIntent().getParcelableExtra("teamSelected");

        imageBadge = (ImageView) findViewById(R.id.imageView);
        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageBadge = (ImageView) findViewById(R.id.imageView);

        updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FetchData fetchdata = new FetchData();
                fetchdata.execute(team);
                /*FetchDataRank fetchDataRank = new FetchDataRank();
                fetchDataRank.execute(team);*/
            }
        });

    }

    @Override
    public void onBackPressed() {
        MainActivity.dbHelper.updateTeam(team);
        //listview a jour
        MainActivity.adapter.notifyDataSetChanged();
        super.onBackPressed();

    }

    public void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

        ImageDownload image = new ImageDownload(imageBadge, this.team);
        image.execute();
    }

    public static String loadContent(Team team, JSONResponseHandlerTeam JSONTeam) {
        JSONTeam = new JSONResponseHandlerTeam(team);
        try {
            URL url = WebServiceUrl.buildSearchTeam(team.getName());
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            JSONTeam.readJsonStream(inputStream);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String loadLastEvent(Team team, JSONResponseHandlerLastEvent JSONEvent) {
        JSONEvent = new JSONResponseHandlerLastEvent(team);
        try {
            URL urlEvent = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlEvent.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            JSONEvent.readJsonStreamLastEvent(inputStream);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String loadRank(Team team, JSONResponseHandlerScoreRank JSONRank) {
        JSONRank = new JSONResponseHandlerScoreRank(team);
        try {
            URL urlRank = WebServiceUrl.buildGetRanking(team.getIdLeague());
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlRank.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            JSONRank.readJsonStreamRank(inputStream);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public class FetchData extends AsyncTask<Team, Void, String> {


        @Override
        protected String doInBackground(Team... teams) {
            JSONResponseHandlerScoreRank JSONRank = new JSONResponseHandlerScoreRank(teams[0]);
            JSONResponseHandlerLastEvent JSONEvent = new JSONResponseHandlerLastEvent(teams[0]);
            JSONResponseHandlerTeam JSONTeam = new JSONResponseHandlerTeam(teams[0]);
            String content = loadContent(teams[0],JSONTeam);
            String event = loadLastEvent(teams[0],JSONEvent);
            String rank = loadRank(teams[0],JSONRank);

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            updateView();
        }


       /* String tv;
        long idChamp,teamId;

        WebServiceUrl weburl,weburl1,weburl2 ;
        URL url,urlRank,urlEvent ;
        JSONResponseHandlerScoreRank JSONRank;
        JSONResponseHandlerTeam JSONTeam;
        JSONResponseHandlerLastEvent JSONEvent ;



        @Override
        protected Team doInBackground(Team... teams) {
            tv = team.getName();
            idChamp = team.getIdLeague();
            teamId = team.getIdTeam();

            JSONRank = new JSONResponseHandlerScoreRank(teams[0]);
            JSONTeam = new JSONResponseHandlerTeam(teams[0]);
            JSONEvent = new JSONResponseHandlerLastEvent(teams[0]);

            try {

                url = weburl.buildSearchTeam(tv);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                JSONTeam.readJsonStream(inputStream);

                urlRank= weburl1.buildGetRanking(idChamp);
                httpURLConnection = (HttpURLConnection) urlRank.openConnection();
                inputStream = httpURLConnection.getInputStream();
                JSONRank.readJsonStreamRank(inputStream);

                urlEvent = weburl2.buildSearchLastEvents(teamId);
                httpURLConnection = (HttpURLConnection) urlEvent.openConnection();
                inputStream = httpURLConnection.getInputStream();
                JSONEvent.readJsonStreamLastEvent(inputStream);


                //return null;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null ;
        }




        @Override
        protected void onPostExecute(Team team) {
            super.onPostExecute(team);
            updateView();
        }
    }


  /*  public class FetchDataRank extends AsyncTask<Team,String,Team>{
        long idChamp;
        WebServiceUrl weburl ;
        URL urlRank ;
        JSONResponseHandlerTeam JSONRank;



        @Override
        protected Team doInBackground(Team... teams) {

            idChamp = team.getIdLeague();
            JSONRank = new JSONResponseHandlerTeam(teams[0]);



            try{
            urlRank= weburl.buildGetRanking(idChamp);
            HttpURLConnection httpURLConnection2 = (HttpURLConnection) urlRank.openConnection();
            InputStream inputStream2 = httpURLConnection2.getInputStream();
            JSONRank.readJsonStream(inputStream2);
               return null;

           } catch (MalformedURLException e) {
               e.printStackTrace();
           } catch (IOException e) {
               e.printStackTrace();
           }

            return null;
        }

        @Override
        protected void onPostExecute(Team team) {
            super.onPostExecute(team);
          updateView();
        }
    }*/


    }

}