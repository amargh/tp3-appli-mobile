package com.example.myapplicationtp3;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerScoreRank {
    private static final String TAG = JSONResponseHandlerScoreRank.class.getSimpleName();
    private Team team;

    public JSONResponseHandlerScoreRank(Team team) {
        this.team = team;
    }

    public void readJsonStreamRank(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {

            readRanks(reader);
        } finally {
            reader.close();
        }
    }

    public void readRanks (JsonReader reader) throws IOException {

        reader.beginObject();

        while (reader.hasNext()) {

            String name = reader.nextName();

            if (name.equals("table")) {
                readArrayScoresRanks(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayScoresRanks(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0;


        while (reader.hasNext() ) {
            reader.beginObject();
            nb = nb+1;

            while (reader.hasNext()) {
                    String name = reader.nextName();
                    if (name != null && name.equals("name")) {

                        String teamName = reader.nextString();
                        String currentTeamName = team.getName();

                        if (teamName != null && teamName.equals(currentTeamName)) {
                            team.setRanking(nb);
                        }

                        while (reader.hasNext()) {
                            String name2 = reader.nextName();

                            if (name2.equals("total")) {
                                team.setTotalPoints(reader.nextInt());
                                break;
                            } else {
                                reader.skipValue();
                            }
                        }
                   } else {
                        if (reader.hasNext()) {
                            reader.skipValue();
                        }
                   }
            }
            reader.endObject();
        }
        reader.endArray();
    }





}
